import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
 
/**
 * A Virus is what my name is. I wish the destruction of the white cells
 * 
 * @author rsolares
 * @version version 1
 */
public class Virus extends Actor 
{
    /**
     * The Virus travel at 4 pixels and turn counter-clockwise 
     * by 1 speed. And it disappers once touching the edge.  
     */
    public void act() 
    {
        setLocation(getX()-8, getY());
        turn(-1);
        
        if (getX() == 0) 
        {
            getWorld().removeObject(this);
        }// end if
    }// end method act    
}// end class Virus
