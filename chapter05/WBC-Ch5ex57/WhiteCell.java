import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This is a white blood cell. This kind of cell has the job to catch 
 * bacteria and remove them from the blood.
 * 
 * @author Michael Kölling
 * @version 0.1 
 */
public class WhiteCell extends Actor
{
    /* FIELDS (INSTANCE VARIABLES) */
    private boolean checkCollision;
    private int points;

     /**
     * Act method for WhiteCell
     */
    public void act() 
    {
        checkKeyPress();
        checkCollision();
        points = 0;
    }// end method act

    /**
     * Check whether a keyboard key has been pressed and react if it has.
     */
    private void checkKeyPress()
    {
        if (Greenfoot.isKeyDown("up")) 
        {
            setLocation(getX(), getY()-8);
        }// end if
        if (Greenfoot.isKeyDown("down")) 
        {
            setLocation(getX(), getY()+8);
        }// end if
        if (Greenfoot.isKeyDown("left")) 
        {
            setLocation(getX()-4, getY());
        }// end if
        if (Greenfoot.isKeyDown("right")) 
        {
            setLocation(getX()+4, getY());
        }// end if
    }// end method checkKeyPress

    /**
     *  The WhiteCell removes Bacteria if it is touching it and the Virus removes the WhiteCell if it touches it.
     */
    private void checkCollision()
    {
        
        if ( isTouching(Bacteria.class) )
        {
            Greenfoot.playSound("firered_0075.wav");// wav file from https://www.sounds-resource.com/game_boy_advance/pokemonfireredleafgreen/sound/4204/
            removeTouching(Bacteria.class); 
            Bloodstream bloodstream = (Bloodstream)getWorld();
            bloodstream.addScore(20); 
        }// end if
        
        if ( isTouching(Virus.class) )
        {
            removeTouching(WhiteCell.class); 
            Bloodstream bloodstream = (Bloodstream)getWorld();
            bloodstream.addScore(-100); 
        }// end if
           
    }// end method checkCollision    
}// end class WhiteCell
