import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * RedCell is my name, I doing nothing yet... I simply travel in the bloodstream. 
 * 
 * @author rsolares
 * @version 1
 */
public class RedCell extends Actor
{
    /* FIELDS (INSTANCE VARIABLES) */
    private int speed;
    /**
     * RedCell moves at a random speed of 1 to 2, disappears once reaching the 
     * edge of the world. 
     */
    public void act() 
    {
        speed = Greenfoot.getRandomNumber(2) + 1;
        setLocation(getX()-speed, getY());
        setRotation(Greenfoot.getRandomNumber(360));
        turn(1);
        
        if (getX() == 0) 
        {
            getWorld().removeObject(this);
        }// end if
    }// end method act    
}// end class RedCell
