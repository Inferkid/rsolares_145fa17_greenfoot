import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The bloodstream is the setting for our White Blood Cell scenario. 
 * It's a place where blood cells, bacteria and viruses float around.
 * 
 * @author rsolares
 * @version 1
 */ 
public class Bloodstream extends World
{
    private int score;
    private int time;
    /**
     * Constructor: Set up the staring objects.
     */
    public Bloodstream() 
    {    
        super(780, 360, 1); 
        prepare();
        setPaintOrder( Border.class ); 
        showScore();
        score = 0;
        time = 2000;
    }// end no-arg constructor for Bloodstream

    /**
     * Create new floating objects at irregular intervals.
     * Bacteria appears from the left at any y value
     * Lining 
     */
    public void act()
    {
        if (Greenfoot.getRandomNumber(100) < 3)// 3 percent chance of showing
        {
            addObject(new Bacteria(), 779, Greenfoot.getRandomNumber(360));
        }// end if 

        if (Greenfoot.getRandomNumber(100) < 1)// one percent chance of showing
        {
            addObject(new Lining(), 779, 360);// appears at the top of the screen
            addObject(new Lining(), 779, 0);//appears at the bottom of the screen
        }// end if

        if (Greenfoot.getRandomNumber(100) < 1)// one percent chance of showing
        { 
            addObject(new Virus(), 779, Greenfoot.getRandomNumber(360));
        }// end if
        
        if (Greenfoot.getRandomNumber(100) < 6)// six percent chance of showing
        { 
            addObject(new RedCell(), 779, Greenfoot.getRandomNumber(360));
        }// end if

    }// end method act
    
    /**
     * Prepare the world for the start of the program. In this case: Create
     * a white blood cell and the lining at the edge of the blood stream.
     */
    private void prepare()
    {
        WhiteCell whitecell = new WhiteCell();
        addObject(whitecell, 100, 179);
        Lining lining = new Lining();
        addObject(lining, 126, 1);
        Lining lining2 = new Lining();
        addObject(lining2, 342, 5);
        Lining lining3 = new Lining();
        addObject(lining3, 589, 2);
        Lining lining4 = new Lining();
        addObject(lining4, 695, 5);
        Lining lining5 = new Lining();
        addObject(lining5, 114, 359);
        Lining lining6 = new Lining();
        Lining lining7 = new Lining();
        addObject(lining7, 295, 353);
        Lining lining8 = new Lining();
        Lining lining9 = new Lining();
        Lining lining10 = new Lining();
        addObject(lining10, 480, 358);
        Lining lining11 = new Lining();
        addObject(lining11, 596, 359);
        Lining lining12 = new Lining();
        addObject(lining12, 740, 354);
        Border border = new Border(); 
        addObject(border, 0, 180); 
        Border border2 = new Border(); 
        addObject(border2, 770, 180);
    }// end method prepare
    
    /**
     * This methods keeps track of the score. 
     */
    public void addScore(int points) 
    {
        score = score + points; 
        showScore();
        if (score < 0 ) 
        {
           Greenfoot.playSound("game-over.wav");
           Greenfoot.stop(); 
        }// end if
    }// end method addScore
    
    /**
     * Show our current score on screen.
     */
    private void showScore()
    {
        showText("Score: " + score, 80, 25);
    }// end method showScore
    
    /**
     * Show our current time on screen.
     */
    private void countTime()
    {
        time--;
        showTime();
        if (time == 0)
        {
            Greenfoot.stop();
        }
    }// end method countTime
    
    /**
     * Show our current time on screen.
     */
    private void showTime()
    {
        showText("Time:" + time, 700, 25);
    }// end method showTime
    
    /**
     * Show a text bubble once the game is over
     */
    private void showEndMessage()
    {
        showText("Your time is over, you lose!", 390, 150);
        showText("Your final score: " + score + "points", 390, 170);
    }// end method showTime
}// end class Bloodstream
