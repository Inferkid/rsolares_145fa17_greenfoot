import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Bacteria fload along in the bloodstream. They are bad. Best to destroy
 * them if you can.
 * 
 * @author Michael Kölling
 * @version 0.1
 */
public class Bacteria extends Actor
{ 
    /* FIELDS (INSTANCE VARIABLES) */
    private int speed;
    /*CONSTRUCTOR*/
    /**
     * Constructor. Nothing to do so far.
     */
    public Bacteria()
    {
        speed = Greenfoot.getRandomNumber(5) + 1;
    }// end no-arg constructor for Bacteria

    /**
     * Float along the bloodstream, slowly rotating. Your movement is now dependent
     * on how much speed you have, speed is between 1 to 3.
     */
    public void act() 
    {
        setLocation(getX()-speed, getY());
        turn(1);
        
        if (getX() == 0) 
        {
            Bloodstream bloodstream = (Bloodstream)getWorld();
            bloodstream.addScore(-15);
            bloodstream.removeObject(this); 
        }// end if
    }// end method act
}// end class Bacteria
