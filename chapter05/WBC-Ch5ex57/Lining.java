import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The Lining are objects at the top and bottom of the screen of the vein.
 * 
 * @author rsolares
 * @version 1
 */
public class Lining extends Actor
{
    /**
     *  Once the lining touches the egde of the world it disappers. 
     */
    public void act() 
    {
        setLocation(getX()-1, getY());// move to the left by 1 pixel
        if (getX() == 0) 
        {
            getWorld().removeObject(this);                               
        }// end if
    }// end method act    
}// end class Lining
