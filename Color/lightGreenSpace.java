import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This object represents the Light Green color of 
 * Robert Plutchik's Wheel of Emotions.
 * 
 * @author rsolares
 * @version 1
 */
public class lightGreenSpace extends Actor
{
    /**
     * Act - do whatever the lightGreenSpace wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }    
}
