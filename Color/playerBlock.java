import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/** 
 * You as the player controll the tile.
 * 
 * @author rsolares
 * @version 1 
 */
public class playerBlock extends Actor
{
    private boolean checkKeys;
    private boolean checkCollision;
    
    private boolean touchedDarkBlue;
    private boolean touchedDarkGreen;
    private boolean touchedLightBlue;
    private boolean touchedLightGreen;
    private boolean touchedOrange;
    private boolean touchedPurple;
    private boolean touchedRed;
    private boolean touchedYellow;
    
    /**
     * Act - do whatever the playerBlock wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        checkKeys();
        while ( touchedDarkBlue != false )
        {
            removeTouching(enemyblockGray.class);
            Greenfoot.playSound("firered_0075.wav");  

        } 
        while ( touchedDarkGreen != false )
        {
            removeTouching(enemyblockBeige.class);
            Greenfoot.playSound("firered_0075.wav");  

        } 
        while ( touchedLightBlue != false )
        {
            removeTouching(enemyblockBlue.class);
            Greenfoot.playSound("firered_0075.wav");  

        } 
        while ( touchedLightGreen != false )
        {
            removeTouching(enemyblockGreen.class);
            Greenfoot.playSound("firered_0075.wav");  

        } 
        while ( touchedOrange != false )
        {
            removeTouching(enemyblockOrange.class);
            Greenfoot.playSound("firered_0075.wav");  

        } 
        while ( touchedPurple != false )
        {
            removeTouching(enemyblockPurple.class);
            Greenfoot.playSound("firered_0075.wav");  

        } 
        while ( touchedRed != false )
        {
            removeTouching(enemyblockRed.class);
            Greenfoot.playSound("firered_0075.wav");  

        } 
        while ( touchedYellow != false )
        {
            removeTouching(enemyblockBlack.class);
            Greenfoot.playSound("firered_0075.wav");  

        } 
        
    }    

    /**
     * Check whether there are any key pressed and react to them.
     */
    private void checkKeys() 
    {
        if(Greenfoot.isKeyDown("up")) {
            setLocation(getX(), getY()+7);  
        }
        if(Greenfoot.isKeyDown("down")) {
            setLocation(getX(), getY()-7);  
        }
        if(Greenfoot.isKeyDown("left")) {
            setLocation(getX()+7, getY());  
        }        
        if(Greenfoot.isKeyDown("right")) {
            setLocation(getX()-7, getY());  
        }    
    }

    /**
     * Check whether we have stumbled upon a color from the wheel. Each color\
     * correpondes to different types of effects. 
     */
    public void lookForWheel()
    {
        if ( isTouching(darkBlueSpace.class) ) 
        {
           touchedDarkBlue = true;
           touchedDarkGreen = false;
           touchedLightBlue = false;
           touchedLightGreen = false;
           touchedOrange = false;
           touchedPurple = false;
           touchedRed = false; 
           touchedYellow = false;

        }// end if
        if ( isTouching(darkGreenSpace.class) ) 
        {
           touchedDarkGreen = true;
           touchedDarkBlue = false;
           touchedLightBlue = false;
           touchedLightGreen = false;
           touchedOrange = false;
           touchedPurple = false;
           touchedRed = false;
           touchedYellow = false;

        }// end if
        if ( isTouching(lightBlueSpace.class) ) 
        {
           touchedLightBlue = true;
           touchedDarkBlue = false;
           touchedDarkGreen = false; 
           touchedLightGreen = false;
           touchedOrange = false;
           touchedPurple = false;
           touchedRed = false;
           touchedYellow = false;

        }// end if
        if ( isTouching(lightGreenSpace.class) ) 
        {
           touchedLightGreen = true;
           touchedDarkBlue = false;
           touchedDarkGreen = false;
           touchedLightBlue = false;
           touchedOrange = false;
           touchedPurple = false;
           touchedRed = false;
           touchedYellow = false;

        }// end if
        if ( isTouching(orangeSpace.class) ) 
        {
           touchedOrange = true;
           touchedDarkBlue = false;
           touchedDarkGreen = false;
           touchedLightBlue = false;
           touchedLightGreen = false;
           touchedPurple = false;
           touchedRed = false;
           touchedYellow = false;

        }// end if
        if ( isTouching(purpleSpace.class) ) 
        {
           touchedPurple = true;
           touchedDarkBlue = false;
           touchedDarkGreen = false;
           touchedLightBlue = false;
           touchedLightGreen = false;
           touchedOrange = false;
           touchedRed = false;
           touchedYellow = false;


        }// end if
        if ( isTouching(redSpace.class) ) 
        {
           touchedRed = true;
           touchedDarkBlue = false;
           touchedDarkGreen = false;
           touchedLightBlue = false;
           touchedLightGreen = false;
           touchedPurple = false;
           touchedOrange = false;
           touchedYellow = false;

        }// end if
        if ( isTouching(yellowSpace.class) ) 
        {
           touchedYellow = true;
           touchedDarkBlue = false;
           touchedDarkGreen = false;
           touchedLightBlue = false;
           touchedLightGreen = false;
           touchedPurple = false;
           touchedRed = false;
           touchedOrange = false;
        }// end if
        
    } // end method lookForWheel

   

}// end method checkCollision 

