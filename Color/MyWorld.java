import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This is the battlefield.
 * 
 * @author rsolares
 * @version 1
 */
public class MyWorld extends World
{
    /* FIELDS */
    private playerBlock player;
    private enemyblockPurple block1;
    private enemyblockBeige block2;
    private enemyblockBlack block3;
    private enemyblockGreen block4;
    private enemyblockGray block5;
    private enemyblockBlue block6;
    private enemyblockRed block7;
    private enemyblockOrange block8;

    private darkBlueSpace space1; 
    private darkGreenSpace  space2; 
    private lightBlueSpace space3; 
    private lightGreenSpace  space4; 
    private orangeSpace space5; 
    private purpleSpace space6; 
    private redSpace space7; 
    private yellowSpace space8; 

    private boolean setupStatus;
    private int strNumberofBliss;
    private int strNumberofRespect;
    private int strNumberofHorror;
    private int strNumberofWonder; 
    private int strNumberofSorrow;
    private int strNumberofHate;
    private int strNumberofFury;
    private int strNumberofWatchful;

    /* CONSTRUCTORS */
    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public MyWorld()
    {    
        // Create a new world with 600x600 cells with a cell size of 1x1 pixels.
        super(750, 750, 1);
        getBackground().setColor(Color.WHITE); 
        getBackground().fill();

        setupStatus = true;
    }// end no-arg constructor for MyWorld

    /**
     * 
     */ 
    public void act()
    { 
        if ( setupStatus ) // more efficient than "if ( setupStatus == true )"
        {
            
            // call the prepare method to add objects to the world
            prepare();

            // pause the game after the worm and lobster objects are added 
            // to the world. (The user must then click the "Run" button to play.)
            Greenfoot.stop();

            setupStatus = false; // prevents "setting up" during subsequent
            // calls to the act method
        } // end if

    }
    /**
     *   
     */
    private void prepare()
    {
        playerBlock player = new playerBlock();
        addObject(player, 350, 350); 
 
        enemyblockPurple block1 = new enemyblockPurple();
        addObject(block1, 50, 50);  
        addObject(block1, 100, 100);
        addObject(block1, 150, 150);
         
        enemyblockBeige block2 = new enemyblockBeige();
        addObject(block2, 0, 500); 
        addObject(block2, 0, 400);
        addObject(block2, 700, 730);
         
        enemyblockBlack block3 = new enemyblockBlack();
        addObject(block3, 600, 650); 
        addObject(block3, 700, 730);
        addObject(block3, 700, 730);
         
        enemyblockBlue block4 = new enemyblockBlue();
        addObject(block4, 600, 650); 
        addObject(block4, 700, 730);
        addObject(block4, 700, 730);
         
        enemyblockRed block5 = new enemyblockRed();
        addObject(block5, 600, 650); 
        addObject(block5, 700, 730);
        addObject(block5, 700, 730);
         
        enemyblockOrange block6 = new enemyblockOrange();
        addObject(block6, 600, 650); 
        addObject(block6, 700, 730);
        addObject(block6, 700, 730);
         
        enemyblockGreen block7 = new enemyblockGreen();
        addObject(block7, 600, 650); 
        addObject(block7, 700, 730);
        addObject(block7, 700, 730);
         
        enemyblockGray block8 = new enemyblockGray();
        addObject(block8, 600, 650); 
        addObject(block8, 700, 730);
        addObject(block8, 700, 730); 
        
        darkBlueSpace space1 = new darkBlueSpace();
        addObject(space1, 359, 437);
        darkGreenSpace space2 = new darkGreenSpace();
        addObject(space2, 444, 354);
        lightBlueSpace space3 = new lightBlueSpace();
        addObject(space3, 416, 414);
        lightGreenSpace space4 = new lightGreenSpace();
        addObject(space4, 422, 293);
        orangeSpace space5 = new orangeSpace();
        addObject(space5, 294, 293);
        purpleSpace space6 = new purpleSpace();
        addObject(space6, 291, 414);
        redSpace space7 = new redSpace();
        addObject(space7, 267, 350);
        yellowSpace space8 = new yellowSpace();
        addObject(space8, 355, 273);
    }   
}

     
    
    

       