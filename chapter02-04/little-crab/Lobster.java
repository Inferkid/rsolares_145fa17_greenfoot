import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class defines a Lobster. Lobster are great to eat!
 */
public class Lobster extends Actor
{

    /**
     * Act - do whatever the Lobster wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        move(5);
        turnAtEdge(); 
        lookforCrab(); 
        randomTurn();
        turnTowards();
    }// end act method   

    /**
     * Check whether we have stumbled upon a Crab.
     * If we have, eat it. If not, do nothing.
     */
    public void lookforCrab()
    {
        if  ( isTouching(Crab.class) )
        {
            removeTouching(Crab.class); 
            Greenfoot.playSound("au.wav");
            Greenfoot.stop(); 
        }// end if    
    }// end method lookFor

    /**
     *  Turns 20 degrees when it hits the edge of the world.
     */
    public void turnAtEdge()
    {
        if  ( isAtEdge() )
        {
            turn(20); 
        }// end if   
    }// end method lookFor

    /**
     *  The lobster randomly turns 45 degress.
     */
    public void randomTurn()
    {
        if  ( Greenfoot.getRandomNumber(100) < 10) 
        {
            turn( Greenfoot.getRandomNumber(91) - 45 );  
        }// end if    
    }// end method lookFor

    /**
     * The Lobster turns to the center of the screen occasionally
     * 30 degrees.
     */
    public void turnTowards()
    {
        if  ( Greenfoot.getRandomNumber(100) < 20)
        {
            turn(30);
        }// end if    
    }//end method turnTowards    
}// end lobster class
