import greenfoot.*;
//comment omitted
/**
 * This class defines a crab. Crabs live on the beach.
 */
public class Crab extends Actor
{
    private GreenfootImage image1;
    private GreenfootImage image2;
    private int wormsEaten;
    private int stepCounter;

    /**
     * For a newly-instantiated Crab object, initialize
     * the state of that onject (by intial its intance varibales)
     */
    public Crab()
    {
        image1 = new GreenfootImage("crab.png");
        image2 = new GreenfootImage("crab2.png");
        setImage(image1);
        stepCounter = 0;
        wormsEaten = 0;

    }//end bo-arg constructor for Crab  

    //methods omitted
    /** 
    /* 
     * Act = do whatever the crabs wants to do. This method is 
     * called whenever the 'Act' or 'Run' button gets pressed
     * on the environment
     */
    public void act()
    {   
        checkKeypress();
        move(5);
        lookForWorm();
        switchImage();
    }//end method act

    /**
     * Check whether we have stumbled upon a worm.
     * If we have, eat it. If not, do nothing.
     */
    public void lookForWorm()
    {
        if  ( isTouching(Worm.class) )
        {
            removeTouching(Worm.class); 
            Greenfoot.playSound("slurp.wav");

            wormsEaten = wormsEaten + 1;
            if (wormsEaten == 8)
            {
                Greenfoot.playSound("fanfare.wav");
                Greenfoot.stop();
            }// end if   
        }// end if 
    }// end method lookFor

    /**
     *  Check whether a control key on the keyboard has been pressed.
     *  If it has, react accordingly.
     */ 
    public void checkKeypress()
    {
        if (Greenfoot.isKeyDown("left"))
        {
            turn (-4);
        }// end if   

        if (Greenfoot.isKeyDown("right"))
        {
            turn (4);
        }// end if 
    }// end method checkKeypress  

    /**
     * Alternates image 1 and 2 to create the animation of
     * the crab moving.
     */
    public void switchImage()
    {
        if ( stepCounter == 3 )
        {
            if ( getImage() == image1 )
            {
                setImage(image2);
            }
            else
            {
                setImage(image1);
            }// end else/if
            stepCounter = 0;
        }// end if
        stepCounter = stepCounter + 1;
    }// end method switchImage
}// end class crab

    
    
    
    
