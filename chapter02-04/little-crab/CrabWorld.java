import greenfoot.*;  // (Actor, World, Greenfoot, GreenfootImage)

public class CrabWorld extends World
{
    private int timeCounter; 

    /**
     * Create the crab world (the beach). Our world has a size 
     * of 560x560 cells, where every cell is just 1 pixel.
     */
    public CrabWorld() 
    {
        super(560, 560, 1);
        Crab littleCrab = new Crab();
        addObject(littleCrab, 125, 485);

        Lobster myLobster = new Lobster();
        addObject(myLobster, 410, 82);
        Lobster myLobster1 = new Lobster();
        addObject(myLobster1, 200, 200);
        Lobster myLobster2 = new Lobster();
        addObject(myLobster2, 415, 367);

        Worm myWorm = new Worm();
        addObject(myWorm, 200, 150);
        Worm myWorm1 = new Worm();
        addObject(myWorm1, 455, 80);
        Worm myWorm2 = new Worm();
        addObject(myWorm2, 100, 150);
        Worm myWorm3 = new Worm();
        addObject(myWorm3, 100, 200);
        Worm myWorm4 = new Worm();
        addObject(myWorm4, 100, 250);
        Worm myWorm5 = new Worm();
        addObject(myWorm5, 100, 300);
        Worm myWorm6 = new Worm();
        addObject(myWorm6, 100, 350);
        Worm myWorm7 = new Worm();
        addObject(myWorm7, 100, 400);
        Worm myWorm8 = new Worm();
        addObject(myWorm8, 250, 255);
        Worm myWorm9 = new Worm();
        addObject(myWorm9, 373, 400);



        prepare();
         
        timeCounter = 500; 
        showText();

    }// end method CrabWorld 

    /**
     * Prepare the world for the start of the program.
     * That is: create the initial objects and add them to the world.
     */
    private void prepare()
    {
    }// end method prepare

    /**
     *  Time counter for the game, once it reaches zero its game over. 
     */
    public void act()
    {
        timeCounter = timeCounter - 1;
        if ( timeCounter == 0 )
        {
            Greenfoot.playSound("slurp.wav");
            Greenfoot.stop(); 
        }// end if
        
    }// end method act
    
    /**
     *  Displays the time that remains for the match near the top left counter 
     */
    public void showText()
    {
        showText("Time left: " +timeCounter, 100, 40);  
        
    }// end method showText
}// end CrabWorld