import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * MyCat is your own cat. Get it to do things by writing code in its act method.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class MyCat extends Cat
{
    /**
     * Act - do whatever the MyCat wants to do.
     */
    public void act()
    {  
     if ( isAlone() ) 
     {
         sleep(1); 
         shoutHooray();
             
          
          

     }//end of line   
     
         
     if ( isBored() ) 
     {
         dance(); 
          

     }//end of line  
     
     if ( isHungry() ) 
     {
         eat(); 
          

     }//end of line 
      
     shoutHooray();
      
    
     
    
    }//end of line 
}//end of line