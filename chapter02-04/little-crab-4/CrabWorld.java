import greenfoot.*;  // (Actor, World, Greenfoot, GreenfootImage)

public class CrabWorld extends World
{
    /**
     * Create the crab world (the beach). Our world has a size 
     * of 560x560 cells, where every cell is just 1 pixel.
     */
    public CrabWorld() 
    {
        super(560, 560, 1);
        
        
        //instantiates a New Crab
        // and then assigns a reference to that object to
        // a new variable called MyCrab, which is of the Crab data type
        Crab myCrab = new Crab();
        
        // add the object reference by MyCrab
        // to the world at he indicated cooridinates
        addObject ( myCrab, 250, 200); 
    }//end CrabWorld constructor
}// end class CrabWorld 
