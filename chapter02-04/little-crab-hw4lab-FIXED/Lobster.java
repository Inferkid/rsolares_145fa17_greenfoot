import greenfoot.*;  // (World, Actor, GreenfootImage, and Greenfoot)

/**
 * A lobster. Lobsters live on the beach. They like to eat crabs. (Well, in our game
 * they do...)
 * 
 * The lobster walks around randomly. If it runs into a crab it eats it.
 * In this version, we have added a sound effect, and the game stops when
 * a lobster eats the crab.
 * 
 * @author your_username@email.uscb.edu
 * @version 145fa17_hw4lab
 */
public class Lobster extends Actor
{
    /* FIELDS */
    private int speed; // default value of an int instance variable is 0

    /* CONSTRUCTORS */
    /**
     * Initializes the instance variables of this Lobster
     */
    public Lobster() 
    {
        speed = 4; 
    } // end no-arg constructor for Lobster    
    
    /* METHODS */
    /**
     * Do whatever lobsters do.
     */
    public void act()
    {
        randomTurn();
        turnAtEdge();
        move(speed);
        lookForCrab();
    } // end method act

    /**
     * Check whether we are at the edge of the world. If so, turn 
     * by a few degrees.
     */
    public void turnAtEdge()
    {
        if ( isAtEdge() )
        {
            turn(17);
        } // end if
    } // end method turnAtEdge

    /**
     * Randomly decide to turn from the current direction, or not. If we turn
     * turn a bit left or right by up to 45 degrees.
     */
    public void randomTurn()
    {
        if (Greenfoot.getRandomNumber(100) < 10) 
        {
            turn(Greenfoot.getRandomNumber(91)-45);
        } // end if
    } // end method randomTurn

    /**
     * Try to pinch a crab. That is: check whether we have stumbled upon a crab.
     * If we have, remove the crab from the game, and stop the program running.
     */
    public void lookForCrab()
    {
        if ( isTouching(Crab.class) ) 
        {
            removeTouching(Crab.class);
            Greenfoot.playSound("au.wav");
            Greenfoot.stop();
        } // end if
    } // end method lookForCrab
} // end class Lobster