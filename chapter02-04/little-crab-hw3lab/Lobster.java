import greenfoot.*;  // (World, Actor, GreenfootImage, and Greenfoot)

/**
 * A lobster. Lobsters live on the beach. They like to eat crabs. (Well, in our game
 * they do...)
 * 
 * The lobster walks around randomly. If it runs into a crab it eats it.
 * In this version, we have added a sound effect, and the game stops when
 * a lobster eats the crab.
 * 
 * @author rsolares@email.uscb.edu, msholmes@email.sc.edu
 * @version 145fa17_hw3lab
 */
public class Lobster extends Actor
{
    /* FIELDS (INSTANCE VARIABLES) */
    private int currentAngle; 
    private int speed;
    /* CONSTRUCTORS */
    /**
     * Lobster Constructer
     */
    public Lobster() 
    {
       currentAngle = Greenfoot.getRandomNumber(360); 
       speed = Greenfoot.getRandomNumber(6) + 3; 
    }// end n0-arg constructor for Lobster   
    
    /**
     * Do whatever lobsters do.
     */
    public void act()
    {
        setRotation( currentAngle); 
        turnAtEdge();
        move(speed);
        lookForCrab();
    } // end method act

    /**
     * Check whether we are at the edge of the world. If we are, turn a bit.
     * If not, do nothing.
     */
    public void turnAtEdge()
    {
        if ( getY() == 0 || getY() == getWorld().getHeight() - 1 )
        {
          int newAngle = 360 - currentAngle ;
          setRotation( newAngle );
        }
        else if ( getX() == 0 || getX() == getWorld().getWidth() - 1 )
        {
            int newAngle = 180 + 360 - currentAngle ;
            setRotation( newAngle );
        } // end if/else

        currentAngle = getRotation(); 
    } // end method turnAtEdge
    
    /**
     * Randomly decide to turn from the current direction, or not. If we turn
     * turn a bit left or right by a random degree.
     */
    public void randomTurn()
    {
        if (Greenfoot.getRandomNumber(100) > 90) 
        {
            turn(Greenfoot.getRandomNumber(90)-45);
        }//end if
    }//end method randomTurn
    
    /**
     * Try to pinch a crab. That is: check whether we have stumbled upon a crab.
     * If we have, remove the crab from the game, and stop the program running.
     */
    public void lookForCrab()
    {
        if ( isTouching(Crab.class) ) 
        {
            removeTouching(Crab.class);
            Greenfoot.playSound("au.wav");
            Greenfoot.stop();
        } // end if
    } // end method lookForCrab
} // end class Lobster